package com.example.appbase;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.example.appbase.domain.db.AppDatabase;
import com.example.appbase.domain.db.dao.UserDao;
import com.example.appbase.domain.entries.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class SimpleEntityReadWriteTest {

    private UserDao mUserDao;
    private AppDatabase mDb;

    @Before
    public void createDb() {
        Context context = ApplicationBase.getContext();
        mDb = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        mUserDao = mDb.userDao();
    }

    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void writeUserAndReadInList() throws Exception {
        User user = new User();
        user.email = "ricoru21@test.com";
        user.userName = "ricoru21";

        mUserDao.insert(user);
        List<User> byName = mUserDao.getAll();
        assertThat(byName.get(0).userName, equalTo(user.userName));
        Log.i("INFO", "Size :: "+byName.size());
    }

}
