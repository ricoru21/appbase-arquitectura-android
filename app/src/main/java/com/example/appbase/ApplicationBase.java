package com.example.appbase;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import com.example.appbase.domain.db.AppDatabase;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationBase extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlyticsKit);
        // Initialize Fabric with the debug-disabled crashlytics.

        //TODO Cambiar la ruta de FontPath
        if(!BuildConfig.DEBUG){
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }

        if (BuildConfig.DEBUG) {
            //Debug Network
            Stetho.initializeWithDefaults(this);
            //memoryLeak - solo está agregado para testear el rendimiento de la aplicación.
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized Context getContext() {
        return context;
    }

    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this);
    }

}
