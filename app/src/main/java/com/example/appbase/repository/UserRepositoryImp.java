package com.example.appbase.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import com.example.appbase.domain.api.ApiUtil;
import com.example.appbase.domain.api.IBaseRemoteService;
import com.example.appbase.domain.db.AppDatabase;
import com.example.appbase.domain.entries.User;

public class UserRepositoryImp implements UserRepository {

    @NonNull
    private IBaseRemoteService mIRemoteService;
    private static UserRepositoryImp sInstance;
    private AppDatabase mDatabase;

    public static UserRepositoryImp getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (UserRepositoryImp.class) {
                if (sInstance == null) {
                    sInstance = new UserRepositoryImp(database);
                }
            }
        }
        return sInstance;
    }

    private UserRepositoryImp(AppDatabase database) {
        mDatabase = database;
        mIRemoteService = ApiUtil.getService();
    }

    @Override
    public Completable insertUser(User user) {
        return Completable.fromAction(() -> {
            mDatabase.userDao().insert(user);
        });
    }

    @Override
    public LiveData<List<User>> getListUser() {
        MediatorLiveData<List<User>> data = new MediatorLiveData<>();
        data.setValue(mDatabase.userDao().getAll());
        return data;
    }

}
