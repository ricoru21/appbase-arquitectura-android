package com.example.appbase.repository;

import android.arch.lifecycle.LiveData;

import java.util.List;

import io.reactivex.Completable;
import com.example.appbase.domain.entries.User;

public interface UserRepository {

    Completable insertUser(User user);
    LiveData<List<User>> getListUser();

}
