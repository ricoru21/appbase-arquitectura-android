package com.example.appbase.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import com.bumptech.glide.Glide;
import com.example.appbase.R;
import com.example.appbase.domain.entries.User;
import com.example.appbase.ui.contract.IBlank;
import com.example.appbase.ui.utils.ExceptionUtil;
import com.example.appbase.viewmodel.UserViewModel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements IBlank {

    private static final String TAG = MainActivity.class.getSimpleName();
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    private UserViewModel mUserViewModel;

    @BindView(R.id.image_profile)
    ImageView imageProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        UserViewModel.Factory userViewModelFactory = new UserViewModel.Factory(getApplication());
        mUserViewModel =
                ViewModelProviders.of(this, userViewModelFactory).get(UserViewModel.class);

        //TODO proceso de prueba
        User user = new User();
        user.email = "ricoru21@gmail.com";
        user.userName = "Ricoru";

        mDisposable.add(mUserViewModel.insertUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        () -> Log.i(TAG, "Registro satisfactorio"),
                        ExceptionUtil::captureException
                ));

        Glide.with(this)
                .load(R.drawable.ic_architecture_google)
                .into(imageProfile);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onStop() {
        super.onStop();
        mDisposable.clear();
    }


    @Override
    public void onClickIBlank() {

    }

}
