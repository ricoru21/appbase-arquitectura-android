package com.example.appbase.ui.utils;

import com.crashlytics.android.Crashlytics;

import com.example.utils.BuildConfig;

public class ExceptionUtil {

    private static final String TAG = ExceptionUtil.class.getSimpleName();

    public static void captureException(Throwable throwable) {
        Crashlytics.logException(throwable);
        if (BuildConfig.DEBUG) throwable.printStackTrace();
    }

    public static void captureException(String log) {
        Crashlytics.logException(new RuntimeException(log));
    }

}
