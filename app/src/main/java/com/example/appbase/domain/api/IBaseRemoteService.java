package com.example.appbase.domain.api;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IBaseRemoteService {

    /*@Headers("Content-Type: application/json")
    @POST("sync/")
    Call<ResponseBody> syncAll(@Header("Authorization") String token, @Body RequestBody body);*/

    @Headers("Content-Type: application/json")
    @POST("sync/")
    Call<ResponseBody> all(@Body RequestBody body);

}
