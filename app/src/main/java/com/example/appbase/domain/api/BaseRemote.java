package com.example.appbase.domain.api;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import com.example.appbase.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRemote {

    public static String TAG = BaseRemote.class.getSimpleName();
    public static String BASE_URL = BuildConfig.BASE_URL;

    public static Retrofit retrofit = null;

    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient.Builder okHttpClient = new OkHttpClient
            .Builder().connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addNetworkInterceptor(new StethoInterceptor());

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()));

    public static void changeBaseUrl(String newApiBaseUrl) {
        BASE_URL = newApiBaseUrl;
        builder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
    }

    public static <S> S create(Class<S> serviceClass) {
        return create(serviceClass, "", "");
    }

    public static <S> S createAuthToken(Class<S> serviceClass, String typeAuth, String tokenAuth) {
        return create(serviceClass, typeAuth, tokenAuth);
    }

    private static <S> S create(Class<S> serviceClass, final String typeAuth, final String tokenAuth) {
        if (!TextUtils.isEmpty(tokenAuth)) {
            AuthenticationInterceptor authInterceptor =
                    new AuthenticationInterceptor(typeAuth, tokenAuth);

            if (!okHttpClient.interceptors().contains(logging)) {
                okHttpClient.addInterceptor(logging);
            }

            if (!okHttpClient.interceptors().contains(authInterceptor)) {
                okHttpClient.addInterceptor(authInterceptor);
            }

            builder.client(okHttpClient.build());

        } else {

            if (okHttpClient.interceptors() != null) {
                okHttpClient.interceptors().clear();

                okHttpClient.addInterceptor(logging);
                builder.client(okHttpClient.build());
            }

        }

        //como vamos a crear nuevamente el retrofit con la intercepción del token
        if (retrofit != null) retrofit = null;

        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }

    /*
     *
     *   Reference: https://futurestud.io/tutorials/android-basic-authentication-with-retrofit
     * */
    private static class AuthenticationInterceptor implements Interceptor {

        private String authToken;
        private String typeAuth;

        private AuthenticationInterceptor(String typeAuth, String authToken) {
            this.authToken = authToken;
            this.typeAuth = typeAuth;
        }

        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder();
            builder.header("Accept", "application/json"); //if necessary, say to consume JSON);
            setAuthHeader(builder, authToken, typeAuth);

            Request request = builder.build();
            return chain.proceed(request);
        }
    }

    private static void setAuthHeader(Request.Builder builder, String authToken, String typeAuth) {
        if (authToken != null) //Add Auth token to each request if authorized
            builder.header("Authorization", typeAuth + " " + authToken);
    }

}
