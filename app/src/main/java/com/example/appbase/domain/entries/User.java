package com.example.appbase.domain.entries;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "m_user")
public class User {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int m_id;

    @ColumnInfo(name = "user_name")
    public String userName;

    @ColumnInfo(name = "email")
    public String email;

}
