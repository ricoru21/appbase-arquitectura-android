package com.example.appbase.domain.api;

public class ApiUtil {

    private static IBaseRemoteService sIRestService;

    public static void changeBaseUrl(String base_url){
        BaseRemote.changeBaseUrl(base_url);
    }

    public static IBaseRemoteService getService() {
        return BaseRemote.create(IBaseRemoteService.class);
    }

    public static IBaseRemoteService getServiceWithToken() {
        if(sIRestService == null){
            //TODO hay que pasar el token_type and token_access
            /*if(userSession!=null){
                sIRestService = BaseRemote.createAuthToken(IBaseRemoteService.class,
                        "token_type", "access_token");
            }*/
        }
        return sIRestService;
    }



}
