package com.example.appbase.domain.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

public class JsonConverter {

    @TypeConverter
    public static HashMap<String, Object> fromString(String value) {
        Type listType = new TypeToken<HashMap<String, Object>>() {}.getType();;
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String mapToString(HashMap<String, Object> data) {
        return new Gson().toJson(data);
    }

}
