package com.example.appbase.domain.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import java.util.List;

import com.example.appbase.domain.entries.User;

@Dao
public interface UserDao extends BaseDao<User> {

    @Query("DELETE FROM m_user")
    void truncateTable(); //nuke

    @Query("SELECT * FROM m_user")
    List<User> getAll();

}

/*Nota: esto es un guía de como operar la tablas en SQLite con Room
* url: https://developer.android.com/training/data-storage/room/accessing-data */