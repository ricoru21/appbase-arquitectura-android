package com.example.appbase.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import com.example.appbase.ApplicationBase;
import com.example.appbase.domain.db.AppDatabase;
import com.example.appbase.domain.entries.User;
import com.example.appbase.repository.UserRepository;
import com.example.appbase.repository.UserRepositoryImp;

public class UserViewModel extends AndroidViewModel {

    private static final String TAG = UserViewModel.class.getSimpleName();

    private UserRepository mUserRepository;

    public UserViewModel(@NonNull Application application, AppDatabase appDatabase) {
        super(application);
        mUserRepository = UserRepositoryImp.getInstance(appDatabase);
    }

    public Completable insertUser(User user){
        return mUserRepository.insertUser(user);
    }

    public LiveData<List<User>> getListUser(){
        return mUserRepository.getListUser();
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @NonNull
        private final Application mApplication;
        @NonNull
        private final AppDatabase mDatabase;

        public Factory(@NonNull Application application) {
            mApplication = application;
            mDatabase =  ((ApplicationBase)application).getDatabase();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new UserViewModel(mApplication,mDatabase);
        }
    }

}
