package com.example.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

public class MessageUtil {

    public static void message(Context context, @StringRes int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void message(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

}
