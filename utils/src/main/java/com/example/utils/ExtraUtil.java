package com.example.utils;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

public class ExtraUtil {

    private static final String SMS_DELIVERED = "DELIVERED";
    public static final int INTENT_REQUEST_UNINSTALL = 9999;

    private static final String TAG = ExtraUtil.class.getSimpleName();

    public static String getUniqueID(Context context) {
        String myAndroidDeviceId = "";
        TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        //ExceptionUtil.captureException(new RuntimeException("No hay permisos READ_PHONE_STATE, para obtener UniqueID "));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return myAndroidDeviceId;
        }
        if (mTelephony.getDeviceId() != null) {
            myAndroidDeviceId = mTelephony.getDeviceId();
        } else {
            myAndroidDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return myAndroidDeviceId;
    }

    public static void callPhone(Context context, String phone) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + (phone)));
        context.startActivity(intent);
    }

    public static void smsPhoneActivity(Context context, String phoneNumber, String message) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phoneNumber));
        intent.putExtra("sms_body", message);
        context.startActivity(intent);
    }

    public static void smsPhoneBackground(Context context, String phoneNumber, String message) throws Exception {
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();

        PendingIntent sentPending = PendingIntent.getBroadcast(context,
                0, new Intent("SENT"), 0);

        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        MessageUtil.message(context, "Mensaje Enviado");
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Log.i(TAG, "Not Sent: Generic failure.");
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Log.i(TAG, "Not Sent: No service (possibly, no SIM-card).");
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Log.i(TAG, "Not Sent: Null PDU.");
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Log.i(TAG, "Not Sent: Radio off (possibly, Airplane mode enabled in Settings).");
                        break;
                    default:
                        Log.i(TAG, "Not Sent: Generic failure.");
                        break;
                }
            }
        }, new IntentFilter("SENT"));

        PendingIntent deliveredPI = PendingIntent.getBroadcast(
                context, 0, new Intent(SMS_DELIVERED), 0);
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "SMS DELIVERED");
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "SMS NOT DELIVERED CANCELED");
                        break;
                    default:
                        Log.i(TAG, "NOT SENT: Generic failure.");
                        break;
                }
            }
        }, new IntentFilter(SMS_DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> mSMSMessage = sms.divideMessage(message);
        for (int i = 0; i < mSMSMessage.size(); i++) {
            sentPendingIntents.add(i, sentPending);
            deliveredPendingIntents.add(i, deliveredPI);
        }
        phoneNumber = "+51" + phoneNumber;
        sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage,
                sentPendingIntents, null);
    }

    public static void irPlayStore(Activity activity, String ir_url) throws Exception {
        try {
            if (ir_url.equalsIgnoreCase("")) {
                ir_url = "market://details?id=" + BuildConfig.APPLICATION_ID;
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ir_url)));
                //activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=pe.wando.miti")));
            } else {
                /*if (!ir_url.startsWith("http://") && !ir_url.startsWith("https://")) ir_url = "http://" + ir_url;*/
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ir_url)));
            }
        } catch (android.content.ActivityNotFoundException e) {
            if (ir_url.equalsIgnoreCase("")) {
                ir_url = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
            }

            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ir_url)));
        }
    }

    public static void installAPK(Activity activity, String path) throws Exception {
        if (!isRooted()) {
            Uri uri = Uri.fromFile(new File(path));
            Intent intent;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //Uri apkUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", toInstall);
                intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                //intent.setData(uri);
                intent.setDataAndType(uri, "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            activity.startActivity(intent);
            MessageUtil.message(activity, "App Installing");
        } else {
            //if your device is rooted then you can install or update app in background directly
            MessageUtil.message(activity, "App Installing...Please Wait");
            File file = new File(path);
            if (file.exists()) {
                String command = "pm install -r " + path;
                //Log.i("IN File exists:", path);
                //Log.i("COMMAND:", command);
                Process proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
                proc.waitFor();
                MessageUtil.message(activity, "App Installed Successfully");
            }
        }
    }

    public static void uninstallAPK(Activity activity, String path) {
        Uri uri = Uri.fromFile(new File(path)); //ACTION_DELETE
        Intent intent_delete = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, Uri.fromParts("package",
                activity.getPackageManager().getPackageArchiveInfo(uri.getPath(), 0).packageName, null));
        activity.startActivity(intent_delete);//, INTENT_REQUEST_UNINSTALL
    }

    public static double getStatusBattery(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        return level / (float) scale;
    }

    private static boolean isRooted() {
        return findBinary("su");
    }

    private static boolean findBinary(String binaryName) {
        String[] places = {
                "/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/",
                "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"
        };
        for (String where : places) {
            if (new File(where + binaryName).exists()) {
                return true;
            }
        }
        return false;
    }

}
