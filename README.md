# DOCUMENTACIÓN ARQUITECTURA DE APLICATIVO MOBILE Y BUENAS PRACTICAS DE DESARROLLO

#### I. INTRODUCCIÓN
La Arquitectura de Desarrollo de aplicativos móviles va a estar implementada bajo la arquitectura de componentes de Google, el tipo de patrón con la cual trabaja es MVVM.

![Arquitectura](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_architecture_google.png?alt=media&token=667fc3dd-7432-4c66-8d8b-063e3a84b5a1)

#### II. Estructura del Proyecto
La estructura interna del proyecto está refleja de la siguiente manera, la cual tiene como base la estructura de la arquitectura antes mencionada.

![Estrcutura Base del Proyecto](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_estructura_proyecto.PNG?alt=media&token=94dc5d65-57f1-448f-b2b2-9c6f41496c86)

> Sugerencia:
> La estructura interna de los proyectos tiene que ser modularizados e independientes, donde la relación principal o la interoperabilidad de los módulos se debería realizar en módulo padre que es el “App”.

#### III. Configuración Base del Proyecto
La configuración base del Proyecto consta de dos pasos a realizar la primera es la configuración del proyecto build.gradle (Project) y la del módulo principal build.gradle (Module: app).

![configuración gradlew](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_configuracion_gradlew.png?alt=media&token=29398a0b-4098-4aae-960f-1be71f7c4284)

A continuación vamos a visualizar una imagen donde muestra la configuración del build.gradle (Project) y build.gradle (Module: app).

###### A. build.gradle (Project)

![Gradle Project](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_gradlew_1.png?alt=media&token=f9323f76-7a53-4d96-9e95-4980a2b09b85)

###### B. build.gradle (Module: app)

![Gradle Module App - 1](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_gradlew_module_1.png?alt=media&token=0ade49cd-9071-4aa4-9921-9ec987f2a527)

![Gradle Module App - 2](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_gradlew_module_1.png?alt=media&token=0ade49cd-9071-4aa4-9921-9ec987f2a527)

![Gradle Module App - 3](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/ic_gradlew_module_1.png?alt=media&token=0ade49cd-9071-4aa4-9921-9ec987f2a527)

#### IV. Uso alternativo de RxJava2 en vez de LiveData
Si deseamos trabajar con RxJava2 en reemplazó de LiveData, debemos tener en cuenta lo siguiente conceptos para poder implementar correctamente en la aplicación.
RxJava 2 presenta varias clases básicas en las que puedes descubrir operadores en:

1. io.reactivex.Flowable (Flowable<T>): Emite 0 o n elementos y termina con un evento de éxito o error. Admite la contrapresión, que permite controlar la rapidez con la que una fuente emite elementos.
2. io.reactivex.Observable (Observable<T>): Emite 0 o n elementos y termina con un evento de éxito o error
3. io.reactivex.Single (Single<T>): Emite un solo elemento o un evento de error. La versión reactiva de un método llamado.
4. io.reactivex.Completable (Maybe<T>): O bien se completa con un éxito o con un evento de error. Nunca emite artículos. La versión reactiva de un Runnable. io.reactivex.Maybe (Completable): Tiene éxito con un elemento, o ningún elemento, o errores. La versión reactiva de un opcional.

#### V. Buenas Prácticas
En esta sección veremos algunos tips de buenas prácticas de desarrollo que Google proporciona al momento de crear una aplicación de software, para tener en cuenta las nomenclaturas a establecer. Ya sea al momento de digitar código, al momento de crear una clase, un paquete, declarar sus atributos o crear componentes nuevos como; drawables, assets, etc.

###### A. Drawable files: 
Es importante tener en cuenta los Prefijos al momento de crear un nuevo drawable file, véase en la siguiente imagen.

![drawable 1](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/drawable_files_1.png?alt=media&token=c9e6eaa3-4e2b-4553-8cef-e925dc44e3b4)

![drawable 2](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/drawable_files_2.png?alt=media&token=b5ba4c83-70dc-4099-b0f0-0a3f9bd0e634)

###### B. Layout files: 
Es importante definir correctamente los nombres de los componentes al crear en la aplicación, eso ayudara a tener una buena legibilidad del código y una identificación más rápida y concisa del mismo.

![layout 1](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/layoutfiles_1.png?alt=media&token=5922e4a5-953e-4e47-9507-eb220e990371)

###### C. Fields definition and naming: 
Una de las recomendaciones que Google brindar al momento de codificar, es que debemos tener en cuenta de cómo se define la nomenclatura de una clase y los fields dentro de una clase.

![fields name](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/field_names.png?alt=media&token=b38be67a-dd99-4358-964c-b2baed4a097e)

###### D. String constants, naming, and values: 
Es importante tener en cuenta la declaración de los elementos.

![fields name](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/string_constants_naming_values.png?alt=media&token=626a303a-5c5c-411f-a009-cd6ed601e742)

![fields name](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/string_constants_naming_values_2.png?alt=media&token=f4109c2b-3720-4e31-a863-060e4b206df4)

###### D. E.	Android Architecture Components: 
Un Ejemplo de las Buenas practicas a desarrollar al momento de implementar la arquitectura de componentes en Android.

![fields name](https://firebasestorage.googleapis.com/v0/b/applicationbase-e1c3b.appspot.com/o/referente_arquitectura_componentes.png?alt=media&token=fc0a3155-2467-4347-8b27-ade49c3c8cce)


#### VI. Enlace de referencia
A.	De Proyecto
1. https://gitlab.com/ricoru21/appbase-arquitectura-android

B.	Enlace de referencia de documentación
1. https://github.com/ribot/android-guidelines/blob/master/project_and_code_guidelines.md
2. https://google.github.io/styleguide/javaguide.html
3. https://jeroenmols.com/blog/2016/03/07/resourcenaming/
4. https://source.android.com/setup/contribute/code-style
5. https://github.com/ReactiveX/RxJava